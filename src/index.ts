import log from "@ajar/marker";
import { saySomething } from "./myModule.js";

const response = saySomething("hello");
log.magenta(response);


/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */

function random() : number;
function random(b:boolean) : number;
function random(b:boolean, max:number) : number;
function random(b:boolean, min:number, max:number) : number;
function random(b:boolean, max:number) : number;
function random(b:boolean, min:number, max:number) : number;

function random(arg1?:boolean, arg2?:number, arg3?:number) {

    if(typeof arg1 === 'boolean') {
        if(typeof arg3 === 'number') {
            // there is 3 parameters
            if(arg1 === true) {
                return Math.random() * ((arg3 as number) - (arg2 as number)) + (arg2 as number);
            } else {
                return Math.round(Math.random() * ((arg3 as number) - (arg2 as number)) + (arg2 as number));
            }
        } else {
            if(typeof arg2 === 'number') {
                // there is 2 parameters
                if(arg1 === true) {
                    return Math.random() * arg2;
                } else {
                    return Math.round(Math.random() * arg2);
                }
            } else {
                // there is only 1 parameter
                if(arg1 === true) {
                    return Math.random();
                } else {
                    return Math.round(Math.random());
                }
            }
        }
    } else {
        // no parameters 
        return Math.round(Math.random());
    }
}

const num1 = random(); // 0 ~ 1 | integer
const num2 = random(true); // 0 ~ 1 | float
const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer
const num5 = random(true, 6); // 0 ~ 6 | float
const num6 = random(true, 2, 6); // 2 ~ 6 | float

console.log({ num1, num2, num3, num4, num5, num6 });

//---------------------------------------------------------------



